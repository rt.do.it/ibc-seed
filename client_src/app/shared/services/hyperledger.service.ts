/**
 * Created by tangross on 16/5/2016.
 */
import { Observable }     from "rxjs/Observable";
import { Injectable }     from "@angular/core";
import { Http, Response } from "@angular/http";

@Injectable()
export class HyperledgerService {
  constructor (private http: Http) {}

  private hyperledgerUrl = "/api/ecaroot2";

  getECARootCertificate (): Observable<Array<string>> {
    return this.http.get(this.hyperledgerUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData (res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error("Response status:" + res.status);
    }
    let body = res.json();
    return body || { };
  }

  private handleError (error: any) {
    let errMsg = error.message || "Server error";
    return Observable.throw(errMsg);
  }
}

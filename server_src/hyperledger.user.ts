/**
 * Created by tangross on 1/5/2016.
 */
import { Ibc }          from "ibm-blockchain-js";
import * as chalk       from "chalk";
import Func = require("ibm-blockchain-js");

import { caConnector }  from "./ca.connector";

const MAX_RETRY   = 5;
const TAG         = "USERMGR";

/**
 * UserManager provides helper class for user management
 */
class UserManager {
  ibc: Ibc;
  chaincode: Object;
  callback: Function;
  caConnector: any;

  /**
   * init function
   * @param {Blockchain} ibc
   * @param {ChainCode} chaincode
   * @param {Function} callback
     */
  public init(ibc, chaincode, callback: Function) {
    [this.ibc, this.chaincode] = [ibc, chaincode];
    this.caConnector = caConnector;
    callback();
  }

  /**
   * login into blockchain network
   * @param id
   * @param secret
   * @param callback
     */
  public login (id: string, secret: string, callback: Function) {
    // todo: this method requires testing
    this.ibc.register(0, id, secret, MAX_RETRY, (err, data) => {
      if (err) {
        console.error(TAG, chalk.red(`Error: ${JSON.stringify(err)}`));
        callback && callback(err);
      } else {
        console.log(TAG, `Register user: ${JSON.stringify(data)}`);
      }
    });
  }

  /**
   * register new user to CA (Depricated: shall be replaced by HLC SDK)
   * @param username
   * @param role
   * @param callback
     */
  public registerUser (username: string, role: number, callback: Function) {
    // todo: this method requires testing
    if (!this.caConnector) {
      callback && callback(new Error("cannot register users before the CA connector is setup!"));
      return;
    }
    let user = { identity: username, role: role }; // afiliation group and account is missing
    console.log(TAG, `Registering user against CA: ${username} | role: ${role}`);
    this.caConnector.registerUser(user, callback);
  }
}

export let userManager = new UserManager();

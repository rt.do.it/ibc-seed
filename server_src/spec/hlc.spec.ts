/**
 * Created by tangross on 22/5/2016.
 */
import { Chain, Member, Hlc } from "hlc-experimental";

let hlc: Hlc      = require("hlc-experimental");
let chain: Chain  = hlc.newChain("testChain");
let fs            = require("fs");

chain.configureKeyValStore( {dir: "/tmp/keyValStore"} );
chain.setMemberServicesUrl("grpc://localhost:50051");

const MEMBER    = "WebAppAdmin";
const PASSWORD  = "DJY27pEnl16d";
const TEST_USER = {
  name: "WebApp_user1",
  role: 1,
  account: "bank_a",
  affiliation: "00001"
};

/**
 * Enroll the WebAppAdmin member. WebAppAdmin member is already registered
 * manually by being included inside the membersrvc.yaml file.
 */
describe("Hyperledger client sdk - Enrol", () => {
  let [webappadmin, crypto, result] = [null, null, null];

  beforeEach((done) => {
    chain.getMember(MEMBER, (err, WebAppAdmin: Member) => {
      if (err) {
        done();
      } else {
        webappadmin = WebAppAdmin;
        WebAppAdmin.enroll(PASSWORD, (err1, c) => {
          if (err1) {
            done();
          } else {
            crypto = JSON.stringify(c);
            // console.log(`Enroll WebAppAdmin member --> ${c.key}`);
            let path = chain.getKeyValStore().dir + "/member." + WebAppAdmin.getName();
            fs.exists(path, exists => {
              if (exists) {
                result = webappadmin;
              }
            });
            console.log("path is:" + path);
            done();
          }
        });
      }
    });
  });

  it("should get and enroll WebAppAdmin", () => {
    expect(webappadmin).not.toBe(null);
    expect(crypto).not.toBe(null);
    expect(result).toEqual(webappadmin);
  });
});

/**
 * Set the WebAppAdmin as the designated chain 'registrar' member who will
 * subsequently register/enroll other new members. WebAppAdmin member is already
 * registered manually by being included inside the membersrvc.yaml file and
 * enrolled in the UT above.
 */
describe("Hyperledger client sdk - set chain registrar", () => {
  let [webappadmin, result] = [null, null];

  beforeEach((done) => {
    chain.getMember(MEMBER, (err, WebAppAdmin: Member) => {
      if (err) {
        done();
      } else {
        webappadmin = WebAppAdmin;
        chain.setRegistrar(WebAppAdmin);
        result = chain.getRegistrar().getName();
        done();
      }
    });
  });

  it("should set chain registrar", () => {
    expect(webappadmin).not.toBe(null);
    expect(result).toEqual(MEMBER);
  });
});

/**
 * Register and enroll a new user with the certificate authority.
 * This will be performed by the registrar member, WebAppAdmin.
 */
describe("Hyperledger client sdk - register and enrol test_user", () => {
  let [testuser, result] = [null, null];

  beforeEach((done) => {
    chain.getMember(TEST_USER, (err, user: Member) => {
      if (err) {
        done();
      } else {
        testuser = user;
        let path = chain.getKeyValStore().dir + "/member." + TEST_USER.name;
        fs.exists(path, exists => {
          if (exists) {
            result = TEST_USER.name;
          }
        });
        done();
      }
    });
  });

  it("should register and enrol test_user", () => {
    expect(testuser).not.toBe(null);
    expect(result).toEqual(TEST_USER.name);
  });
});

/**
 * Created by tangross on 13/5/2016.
 */
import { caConnector } from "../ca.connector";

describe("CA-connector-ECA", () => {
  let [err, cert] = [null, null];

  beforeEach((done) => {
    caConnector.getECACertificate((e, c) => {
      [err, cert] = [e, c];
      done();
    });
  });

  it("should return ECA root cert", () => {
    expect(cert).not.toBe(null);
  });
});

describe("CA-connector-TCA", () => {
  let [err, cert] = [null, null];

  beforeEach((done) => {
    caConnector.getTCACertificate((e, c) => {
      [err, cert] = [e, c];
      done();
    });
  });

  it("should return TCA root cert", () => {
    expect(cert).not.toBe(null);
  });
});

describe("CA-connector-TLS", () => {
  let [err, cert] = [null, null];

  beforeEach((done) => {
    caConnector.getTLSCertificate((e, c) => {
      [err, cert] = [e, c];
      done();
    });
  });

  it("should return TLS root cert", () => {
    expect(cert).not.toBe(null);
  });
});

describe("CA-connector-Register User", () => {
  let [err, response] = [null, null];
  let testUser = {
    identity: "testUser" + Date.now(),
    role: 1,
    account: "bank_a",
    affiliation: "00001"
  };

  beforeEach((done) => {
    caConnector.registerUser(testUser, (e, r) => {
      [err, response] = [e, r];
      done();
    });
  });

  it("should return token", () => {
    expect(response).not.toBe(null);
  });
});


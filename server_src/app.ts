import * as express           from "express";
import * as path              from "path";
import * as bodyParser        from "body-parser";
import * as logger            from "morgan";
import * as http              from "http";
import * as cookieParser      from "cookie-parser";
import * as chalk             from "chalk";
import * as cors              from "cors";
import * as socketIO          from "socket.io";
import { Request, Response }  from "express";

import { blockchain }         from "./hyperledger.setup";
import { userManager }        from "./hyperledger.user";
import * as router            from "./routes/index";

let config  = { root: "" };
let app     = express();
let env     = process.env.env || "dev";

config.root = (env !== "prod") ? path.join(__dirname, "../dev") : path.join(__dirname, "../prod");

app.disable("x-powered-by");

if (env === "dev") {
  app.use("/", express.static(config.root));
  app.use("/dist/dev", express.static(config.root));
  app.use("/node_modules", express.static(path.join(__dirname, "../../node_modules")));
} else {
    app.use(express.static(config.root));
}

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.options("*", cors());
app.use(cors());
app.use("/", router);

// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: Function) => {
  let err: any = new Error("Not Found");
  err.status = 404;
  next(err);
});

if (env === "dev") {
  app.use((err: any, req: Request, res: Response, next: Function) => {
    res.status(err.status || 500);
    console.error(JSON.stringify(err));
    res.send(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err: any, req: Request, res: Response) => {
  res.status(err.status || 500);
});

let port = process.env.port || "5556";  // todo: move it config.ts
app.set("port", port);

// deploy chaincode
blockchain.init(blockchainReady);

function blockchainReady(ibc, cc) {
  userManager.init(ibc, cc, caReady);
}

function caReady() {
  let server = http.createServer(app);
  console.log(chalk.yellow("-----------------------------"));
  console.log(chalk.yellow(`ENVIRONMENT - ${env}`));
  console.log(chalk.yellow(`HOST        - 127.0.0.1`));
  console.log(chalk.yellow(`PORT        - ${port}`));
  console.log(chalk.yellow("-----------------------------"));
  // socketIO should be replaced by RxJS implementation
  let io = socketIO(server);
  io.on("connection", socket => {
    console.log(`user connected`);
    socket.on("event", message => {
      console.log(`received message: ${message}`);
      // let data = JSON.parse(message);
    });
    socket.on("disconnect", () => {
      console.log(`user disconnected`);
    });
  });
  server.listen(port);
}

import * as chalk from "chalk";

let config: Config = {
  network: {
    "peers": [
      {
        "discovery_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp1-discovery.blockchain.ibm.com",
        "discovery_port": 30303,
        "api_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp1-api.blockchain.ibm.com",
        "api_port_tls": 443,
        "api_port": 80,
        "type": "peer",
        "network_id": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e",
        "container_id": "05582430b63d0671949b7fc5dd346d33ba6c9a3fb7662c5e315ccc1482943f28",
        "id": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp1",
        "api_url": "http://e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp1-api.blockchain.ibm.com:80"
      },
      {
        "discovery_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp2-discovery.blockchain.ibm.com",
        "discovery_port": 30303,
        "api_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp2-api.blockchain.ibm.com",
        "api_port_tls": 443,
        "api_port": 80,
        "type": "peer",
        "network_id": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e",
        "container_id": "a8bc8a22f86010140434933398b1c15e3d795107e4cbed67ff1db8bfe10dae36",
        "id": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp2",
        "api_url": "http://e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_vp2-api.blockchain.ibm.com:80"
      }
    ],
    "ca": {
      "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_ca": {
        "url": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_ca-api.blockchain.ibm.com:30303",
        "discovery_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_ca-discovery.blockchain.ibm.com",
        "discovery_port": 30303,
        "api_host": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e_ca-api.blockchain.ibm.com",
        "api_port_tls": 30303,
        "api_port": 80,
        "type": "ca",
        "network_id": "e06fa12d-df0e-4dd1-93c8-236eaefc8d1e",
        "container_id": "1df9e4517c9e8b39849e89092957a723113956a37748ef0e06e00a1a127fd97d"
      }
    },
    "users": [
      {
        "username": "dashboarduser_type0_a181388233",
        "secret": "f2bb431a2e",
        "enrollId": "dashboarduser_type0_a181388233",
        "enrollSecret": "f2bb431a2e"
      },
      {
        "username": "dashboarduser_type0_23e6897702",
        "secret": "f409b86be4",
        "enrollId": "dashboarduser_type0_23e6897702",
        "enrollSecret": "f409b86be4"
      },
      {
        "username": "user_type1_9289be2c7b",
        "secret": "87ef572fda",
        "enrollId": "user_type1_9289be2c7b",
        "enrollSecret": "87ef572fda"
      },
      {
        "username": "user_type1_cda93cea70",
        "secret": "a148c0042e",
        "enrollId": "user_type1_cda93cea70",
        "enrollSecret": "a148c0042e"
      },
      {
        "username": "user_type1_63913b0f31",
        "secret": "6cfa8aa832",
        "enrollId": "user_type1_63913b0f31",
        "enrollSecret": "6cfa8aa832"
      },
      {
        "username": "user_type1_c344831fdf",
        "secret": "1083dec5a4",
        "enrollId": "user_type1_c344831fdf",
        "enrollSecret": "1083dec5a4"
      },
      {
        "username": "user_type2_ea4b0ee932",
        "secret": "732b5f69dc",
        "enrollId": "user_type2_ea4b0ee932",
        "enrollSecret": "732b5f69dc"
      },
      {
        "username": "user_type2_37ad07748b",
        "secret": "a6aa826196",
        "enrollId": "user_type2_37ad07748b",
        "enrollSecret": "a6aa826196"
      },
      {
        "username": "user_type2_210c10beb5",
        "secret": "c33ac2cb62",
        "enrollId": "user_type2_210c10beb5",
        "enrollSecret": "c33ac2cb62"
      }
    ]
  },
  "chaincode": {
    "zip_url": "https://github.com/IBM-Blockchain/cp-chaincode-v2/archive/master.zip",
    "unzip_dir": "cp-chaincode-v2-master/hyperledger",
    "git_url": "https://github.com/IBM-Blockchain/cp-chaincode-v2/hyperledger"
    // deployed_name:
    //   "2450c95bc77e124c766ff650c2f4642e5c0bc2d576ee67db130900750cddc5982e295f320fd5dff7aca2f61fa7cc673fcdcc8a7464f94c68eeccdb14b2384a75"
  }
};

const SERVICE_NAME = "ibm-blockchain-5-prod";

interface Config {
  network?: any;
  chaincode?: any;
}

function getConfig (): Config {
  if (process.env.VCAP_SERVICES) {
    let servicesObject = JSON.parse(process.env.VCAP_SERVICES);
    for (let obj of servicesObject) {
      if (obj.indexOf(SERVICE_NAME) >= 0) {
        if (obj[0].credentials.error) {
          console.log(chalk.red(`Error from Bluemix: ${obj[0].credentials.error}`));
          // process.error = {type: "network", msg: "Bluemix network service error"};
        }
      }
      if (obj[0].credentials && obj[0].credentials.peers) {
        console.log(chalk.green(`overwriting peers; loading from vcap service`));
        config.network.peers = obj[0].credentials.peers;
        // this.peers = obj[0].credentials.peers;
        let ca_name = Object.keys(obj[0].credentials.ca)[0];
        console.log(chalk.green(`loading ca: ${ca_name}`));
        config.network.ca = obj[0].credentials.ca[ca_name];
        if (obj[0].credentials.users) {
          console.log(chalk.green(`overwritting users; loading from vcap service`));
          // this.users = obj[0].credentials.users;
          config.network.users = obj[0].credentials.users;
        }
      }
    }
  }
  return config;
}

export const BLOCKCHAIN = getConfig();

export const CC_SUMMARY         = "./cc_summaries";
export const CC_INIT            = "init";
export const CC_INIT_PARAMS     = [""];
export const ENROLL_ID          = null;
export const isCaLocalhost      = true;
export const CA_HOST            = "127.0.0.1";
export const CA_PORT            = 50051;
// export const isCAConnectSecure  = false;
// export const CC_ADDRESS     = "";

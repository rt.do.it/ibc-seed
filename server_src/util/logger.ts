/**
 * Created by tangross on 19/5/2016.
 */
// let bunyan = require("bunyan");
//
// function Logger(config) {
//   // config is the object passed to the client constructor.
//   let bun = bunyan.createLogger({name: "mylogger"});
//   this.error = bun.error.bind(bun);
//   this.warning = bun.warn.bind(bun);
//   this.info = bun.info.bind(bun);
//   this.debug = bun.debug.bind(bun);
//   this.trace = function (method, requestUrl, body, responseBody, responseStatus) {
//     bun.trace({
//       method: method,
//       requestUrl: requestUrl,
//       body: body,
//       responseBody: responseBody,
//       responseStatus: responseStatus
//     });
//   };
//   this.close = function () { /* bunyan"s loggers do not need to be closed */ };
// }
//
// export { Logger };

// see https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/logging.html

/**
 * Created by tangross on 2/5/2016.
 */
import * as config from "./config";

let debug               = require("debug")("CAconnector");
let grpc: Grpc          = require("grpc");
let jsrsa: Jsrsa        = require("jsrsasign");
let KEYUTIL             = jsrsa.KEYUTIL;
let asn1                = jsrsa.asn1;
// let sha3_256 = require("js-sha3").sha3_256;
let protoFile           = __dirname + "/protos/ca.proto";
let Timestamp           = grpc.load(__dirname + "/protos/google/protobuf/timestamp.proto").google.protobuf.Timestamp;
let elliptic: IElliptic = require("elliptic");

const TAG = "CERTAUTH";

interface Jsrsa {
  KEYUTIL?: { generateKeypair(arg1: string, arg2: string) };
  // asn1?: { x509: { SubjectPublicKeyInfo() } } ;
  asn1?: any;
}
interface EcKeypair { pubKeyObj?: { pubKeyHex }; prvKeyObj?: { prvKeyHex }; }
interface Ecdsa { keyFromPublic?: Function; keyFromPrivate?: Function; sign?: Function; }
interface Spki { getASN1Object?: () => { getEncodedHex() }; }
interface IElliptic { ec: any; curves: any; }
interface ProtoDescriptor {
  ECAA?: any; ECAP?: any; TCAP?: any; TLSCAP?: any; RegisterUserReq?: any; Empty?: any; ECertCreateReq?: any;
  CryptoType?: { ECDSA };
  PublicKey?: any;
  Signature?: any;
}
interface Grpc {
  // load?: (path: string) => { google: { protobuf: { Timestamp } }, protos };
  load?: any;
  credentials?: {createSsl: Function, createInsecure: Function};
}
interface IECAA { registerUser(registerUserRequest: RegisterUserReq, callback?: Function); }
interface IECAP { createCertifcatePair: Function; readCaCertificate: Function; }
interface ITCAP { readCaCertificate: Function; }
interface ITLSCAP { readCaCertificate: Function; }
interface UserRequest {
  identity: string;
  role: number;
  account: string;
  affiliation: string;
}

interface RegisterUserReq {
  setId(id: Object): void;
  setRole(role: number): void;
  setAccount(account: string): void;
  setAffiliation(affiliation: string): void;
}

interface IECertCreateReq {
  setTs(ts: Object): void;
  setId(id: Object): void;
  setTok(tok: Object): void;
  setEnc(key: Object): void;
  setSign(key: Object): void;
  setSig(signature: Object): void;
}

/**
 * Helper class to connect Certificate Authority
 * @constructor
 */
class CAConnector {
  name: string = "OBCConnector";
  grpcServerAddress: string;
  grpcCredentials;
  protos: ProtoDescriptor;
  ecaaClient: IECAA;
  ecapClient: IECAP;
  tcapClient: ITCAP;
  tlscapClient: ITLSCAP;

  constructor() {
    let ca = config.BLOCKCHAIN.network.ca;
    if (config.isCaLocalhost) {
      this.grpcServerAddress = `${config.CA_HOST}:${config.CA_PORT}`;
      this.grpcCredentials = grpc.credentials.createInsecure();
      // todo: createSsl does not works, for localhost dev
    } else {
      Object.keys(ca).forEach( key => {
        // todo: below does not works
        this.grpcServerAddress = ca[key].url;
        // this.grpcServerAddress = "504f6bfe-ead0-4bd2-b77c-b8bfe88cfcea_ca-api.blockchain.ibm.com:30303";
        this.grpcCredentials = grpc.credentials.createSsl();
      });
    }
    console.log(TAG, `Server address: ${this.grpcServerAddress}`);
    this.protos = grpc.load(protoFile).protos;
    this.ecapClient = new this.protos.ECAP(this.grpcServerAddress, this.grpcCredentials);
    this.tcapClient = new this.protos.TCAP(this.grpcServerAddress, this.grpcCredentials);
    this.ecaaClient = new this.protos.ECAA(this.grpcServerAddress, this.grpcCredentials);
    this.tlscapClient = new this.protos.TLSCAP(this.grpcServerAddress, this.grpcCredentials);
  }

  /**
   * register a new user with membership services
   * @param {Object} userRequest
   * @param {Function} callback
     */
  public registerUser (userRequest: UserRequest, callback: Function) {
    let registerUserRequest: RegisterUserReq = new this.protos.RegisterUserReq();
    registerUserRequest.setId({ id: userRequest.identity });
    registerUserRequest.setRole(userRequest.role);
    registerUserRequest.setAccount(userRequest.account);
    registerUserRequest.setAffiliation(userRequest.affiliation);
    this.ecaaClient.registerUser(registerUserRequest, (err, token: any) => {
      if (err) {
        return callback(err, null);
      } else {
        console.log(TAG, `Register new user: ${userRequest.identity} | token: ${token.tok.toString()}`);
        return callback(null, {identity: userRequest.identity, token: token.tok.toString()});
      }
    });
  }

  /**
   * get ECA Root Certificate
   * @param {Function} callback
    */
  public getECACertificate (callback: Function) {
    this.ecapClient.readCaCertificate(new this.protos.Empty(), (err, cert) => {
      if (err) {
        console.error(TAG, `Fail: get ECA root cert: ${JSON.stringify(err)}`);
        return callback(err, null);
      } else {
        // console.log(TAG, `ECA Root Cert: ${cert.cert.toString("hex")}`);
        // console.log(TAG, `Get ECA root cert`);
        return callback(null, cert.cert.toString("hex"));
      }
    });
  }

  /**
   * get TCA Root Certifcate
   * @param {Function} callback
    */
  public getTCACertificate (callback: Function) {
    this.tcapClient.readCaCertificate(new this.protos.Empty(), (err, cert) => {
      if (err) {
        return callback(err, null);
      } else {
        // console.log(TAG, `TCA Root Cert: ${cert.cert.toString("hex")}`);
        // console.log(TAG, `Get TCA root cert`);
        return callback(null, cert.cert.toString("hex"));
      }
    });
  }

  /**
   * get TLS Root Certificate
   * @param callback
     */
  public getTLSCertificate (callback: Function) {
    this.tlscapClient.readCaCertificate(new this.protos.Empty(), (err, cert) => {
      if (err) {
        console.error(TAG, `Fail: get TLS root cert`);
        return callback(err, null);
      } else {
        // console.log(TAG, `TLS Root Cert: ${cert.cert.toString("hex")}`);
        // console.log(TAG, `Get TLS root cert`);
        return callback(null, cert.cert.toString("hex"));
      }
    });
  }

  public getEnrollmentCertificateFromECA (loginRequest, callback: Function) {
    let timestamp = new Timestamp({seconds: Date.now() / 1000, nanos: 0});
    // generate ECDSA keys
    let ecKeypair: EcKeypair = KEYUTIL.generateKeypair("EC", "secp256r1");
    let spki: Spki = new asn1.x509.SubjectPublicKeyInfo(ecKeypair.pubKeyObj);
    let ecKeypair2: EcKeypair = KEYUTIL.generateKeypair("EC", "secp256r1");
    let spki2: Spki = new asn1.x509.SubjectPublicKeyInfo(ecKeypair2.pubKeyObj);

    // create the proto message
    let eCertCreateRequest: IECertCreateReq = new this.protos.ECertCreateReq();
    eCertCreateRequest.setTs(timestamp);
    eCertCreateRequest.setId({ id: loginRequest.identity });
    eCertCreateRequest.setTok({ tok: new Buffer(loginRequest.token)} );

    // public signing key (ecdsa)
    let signPubKey = new this.protos.PublicKey({
      type: this.protos.CryptoType.ECDSA,
      key: new Buffer(spki.getASN1Object().getEncodedHex(), "hex")
    });
    eCertCreateRequest.setSign(signPubKey);

    // public encryption key (ecdsa)
    let encPubKey = new this.protos.PublicKey({
      type: this.protos.CryptoType.ECDSA,
      key: new Buffer(spki2.getASN1Object().getEncodedHex(), "hex")
    });
    eCertCreateRequest.setEnc(encPubKey);

    this.createCertificatePair(eCertCreateRequest, (err, eCertCreateResp) => {
      if (!err) {
        let cipherText = eCertCreateResp.tok.tok;
        // cipherText = ephemeralPubKeyBytes + encryptedTokBytes + macBytes
        // ephemeralPubKeyBytes = first ((384+7)/8)*2 + 1 bytes = first 97 bytes
        // ephemeralPubKeyBytes = first ((256+7)/8)*2 + 1 bytes = first 65 bytes
        // hmac is sha3_384 = 48 bytes or sha3_256 = 32 bytes
        let ephemeralPublicKeyBytes = cipherText.slice(0, 65); // 256: 65, 384: 97
        let encryptedTokBytes = cipherText.slice(65, cipherText.length - 32);
        let macBytes = cipherText.slice(cipherText.length - 48);
        debug("length = ", ephemeralPublicKeyBytes.length + encryptedTokBytes.length + macBytes.length);
        let EC = elliptic.ec;
        let format = "p256";
        let curve = elliptic.curves[format];
        let ecdsa: Ecdsa = new EC(curve);

        // convert bytes to usable key object
        let ephPubKey = ecdsa.keyFromPublic(ephemeralPublicKeyBytes.toString("hex"), "hex");
        let encPrivKey = ecdsa.keyFromPrivate(ecKeypair2.prvKeyObj.prvKeyHex, "hex");
        console.log(`ephPubKey: ${ephPubKey}; encPrivKey: ${encPrivKey}`);

        // do not understand
        // let secret = encPrivKey.derive(ephPubKey.getPublic());

      }
    });
  }

  private createCertificatePair (eCertCreateRequest: IECertCreateReq, callback: Function) {
    this.ecapClient.createCertifcatePair(eCertCreateRequest, (err, eCertCreateResp) => {
      if (err) {
        console.error(TAG, `Error: eCertCreateRequest: ${err}`);
        callback(err, null);
      } else {
        callback(null, eCertCreateResp);
      }
    });
  }
}

export let caConnector = new CAConnector();

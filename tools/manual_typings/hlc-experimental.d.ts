/**
 * Created by tangross on 23/5/2016.
 */
// https://github.com/hyperledger/fabric/blob/master/sdk/node/hlc.js

declare module 'hlc-experimental' {

  export class Chain {
    constructor(name: string);
    getName(): string;
    addPeer(endpoint: Endpoint): Peer;
    getPeers(): Peer[];
    getRegistrar(): Member;
    setRegistrar(registrar: Member): any; // todo: not implemented
    setMemberServicesUrl(url: string): void;
    getMemberServices(): MemberServices;
    setMemberServices(memberServices: MemberServices): void;
    configureKeyValStore(config: Object): void;
    getKeyValStore(): any;
    setKeyValStore(keyValStore: any): void;
    getMember(name: Object|string, cb: Function): any;
    sendTransaction(tx: TransactionContext, eventEmitter: any): void;
  }

  export class Member {
    constructor(cfg: Object|string, chain: Chain);
    getName(): string;
    getRole(): number;
    getAccount(): string;
    getAffiliation: string;
    getChain(): Chain;
    getTCertBatchSize(): number;
    setTCertBatchSize(batchSize: number): void;
    isRegistered(): boolean;
    isEnrolled(): boolean;

    /**
     * Register the memberr
     * @param registrar Registration request with the following fields: name (EnrollmentID),
     * role (1:client, 2: peer, 4: validator, 8: auditor), account and affiliation.
     * @param cb Callback of the form: {function(err,enrollmentSecret)}
       */
    register(registrar: Object, cb: Function): void;

    /**
     * Enroll the member and return the enrollment results.
     * @param enrollmentSecret enrollmentSecret The enrollment secret as returned by register.
     * @param cb Callback of the form: {function(err,{key,cert,chainKey})}
       */
    enroll(enrollmentSecret: string, cb: Function): void;
    registerAndEnroll(registrar: Object, cb: Function): void;

    /**
     * Issue a build request on behalf of this member.
     * @param buildRequest
     * @returns {TransactionContext} Emits 'submitted', 'complete', and 'error' events.
       */
    build(buildRequest: Object): TransactionContext;
    deploy(deployRequest: Object): TransactionContext;
    invoke(invokeRequest: Object): TransactionContext;
    query(queryRequest: Object): TransactionContext;
    newTransactionContext(tcert: Object): TransactionContext;
    getNextTCert(cb: Function): void;

    /**
     * Save the state of this member to the key value store.
     * @param cb Callback of the form: {function(err}
       */
    saveState(cb: Function): void;

    /**
     * Restore the state of this member from the key value store (if found).  If not found, do nothing.
     * @param cb Callback of the form: {function(err}
       */
    restoreState(cb: Function): void;

    /**
     * Save the current state of this member as a string
     * @param str The state of this member as a string
       */
    fromString(str: string): string;
    toString(): string;
  }

  export class TransactionContext {
    getMember(): Member;
    getChain(): Chain;

    // todo: Below are not implemented method; need to review the source later.
    build(buildRequest: Object): any;
    deploy(deployRequest: Object): any;
    invoke(invokeRequest: Object): any;
    query(queryRequest: Object): any;
  }

  export class Peer {
    constructor(url: string, chain: Chain);
    getChain(): Chain;
    getUrl(): string;
    sendTransaction(tx: TransactionContext, eventEmitter: any): void;
    remove(): void;
  }

  export class Endpoint {
    constructor(url: string);
  }

  export class MemberServices {
    constructor(url: string);

    /**
     * Register the member and return an enrollment secret (one time password).
     * @param req Registration request with the following fields: name (EnrollmentID),
     * role (1:client, 2: peer, 4: validator, 8: auditor), account and affiliation.
     * @param cb Callback of the form: {function(err,enrollmentSecret)}
       */
    register(req: Object, cb: Function): void;

    /**
     * Enroll the member and return an opaque member object
     * @param req Enrollment request with the following fields: name, enrollmentSecret
     * @param cb Callback of the form: {function(err,{key,cert,chainKey})}
     */
    enroll(req: Object, cb: Function): void;

    /**
     * Get an array of transaction certificates (tcerts).
     * @param req Request of the form: {name,enrollment,num} where
     * 'name' is the member name,
     * 'enrollment' is what was returned by enroll, and
     * 'num' is the number of transaction contexts to obtain.
     * @param {function(err,[Object])} cb Callback which is called with an error as 1st arg and an array of tcerts as 2nd arg.
     */
    getTransactionCerts(req: Object, cb: Function): void;
  }

  export class Hlc {
    newChain(name: string): Chain;
    getChain(chainName: string, create: boolean): Chain;
    newMemberServices(url: string): MemberServices;
    bluemixInit(): boolean;
  }

  // exports.Hlc = Hlc;
  // exports.Chain = Chain;
  // exports.Member = Member;
}


// beware that this is partially implemented

declare module "ibm-blockchain-js" {

    interface Details {
      deployed_name?:string;
      git_url?:string;
      peers?:string[];
      timestamp?:number;
      users?:string[];
      unzip_dir?:string;
      zip_url?:string;
      func?: any;
    }

    export class Error {
      name: string;
      code: number;
      details: Details;
    }

    export class ChainCode {
      query: any;
      invoke: any;
      deploy: Function;
      details: Details;
    }

    export class Ibc {
      load (options:Object, callback?: Function): void;
      load_chaincode (options:Object, callback?: Function): any;
      network (arrayPeer:Object, options?: Object): void;
      register (peerIndex: number, enrollID: string, enrollsecret: string, maxRetry?: number, callback?: Function): void;
      save (path: string): void;
    }
}

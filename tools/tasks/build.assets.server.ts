import {join} from "path";
import {SERVER_SRC, SERVER_DEST} from "../config";

export = function buildAssetsDev(gulp, plugins) {
  return function () {
    return gulp.src([
        join(SERVER_SRC, "**"),
        "!" + join(SERVER_SRC, "**", "*.ts")
      ])
      .pipe(gulp.dest(SERVER_DEST));
  };
}
